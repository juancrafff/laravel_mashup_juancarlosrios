@extends('layouts.master')

@section('titulo')
Resultados
@endsection

@section('contenido')
<div class="container">
	<div class="row">
		<div class="col-md-5">
			<div class="card mb-4 box-shadow">
				<img class="card-img-top" src="{{ asset("assets/imagenes/") }}/{{ $imagen->imagen }}">
				<div class="card-body">
					<h4 class="card-text text-center text-uppercase">{{ $imagen->titulo }}</h4>
					<div class="d-flex justify-content-between align-items-center">
						<small class="text-muted">{{ $imagen->created_at }}</small>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-7">
			<h2 class="text-center">La AI ha detectado:</h2>
			<div class="row">
				@foreach ($tags as $tag)
				<div div class="col-md-2">
					<button href="#top" type="button" class="btn btn-sm btn-outline-secondary" id="{{$tag->tag->en}}">#{{ $tag->tag->es}}</button>
				</div>
				@endforeach
				<a href=""></a>
			</div>
		</div>
	</div>
</div>

<div class="album py-5 bg-light">
	<div class="container">
		<div class="row" id="resultados" id="top">
			<h4 class="text-center">¡Haz click en alguno de los hashtags para ver la imágenes relacionadas!</h4>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(document).ready( function()
	{
		$( "button" ).click(function()
		{
			buscarImagenes(this.id);
		}
		);

		function buscarImagenes(busqueda)
		{
			var clave = "CS99whCSpWsOBR6Z82gTWhD6qnzvQwZQBOiSRWwDo-8";
			var resultado = "";
			console.log('He entrado a la función para la palabra: '+busqueda);
			$("#resultados").empty();

			$.get("https://api.unsplash.com/search/photos?page=1&query=" + busqueda + "&client_id="+clave,
				function(data)
				{
					console.log(data);
					if(data.results.length == 0)
					{
						resultado = "<h3>No hay resultados</h3>";
					}
					else
					{
						$("#resultados").append("<h3 class='text-center text-uppercase'>Se han encontrado los siguientes resultados</h3>")
						data.results.forEach((item) =>
						{
							console.log(item.links.html);
							resultado = `
							<div class='col-md-4'>
								<div class='card mb-4 box-shadow'>
									<img class='card-img-top' src='${item.urls.small}'>
									<div class='card-body'>
										<h6 class='card-text text-uppercase'>${item.alt_description}</h6>

										<small class='text-muted'>${item.description}</small>
										<div class='d-flex justify-content-between align-items-center'>
											<div class="btn-group">
												<a href="${item.links.download}" class="btn btn-sm btn-outline-primary" target="_blank" download>Descargar</a>
												<a href="${item.links.html}" class="btn btn-sm btn-outline-secondary" target="_blank">Ver más</a>
											</div>
										</div>
									</div>
								</div>
							</div>`;
							$("#resultados").append(resultado);
						});
					}
				});
		}
	});

</script>
@endsection