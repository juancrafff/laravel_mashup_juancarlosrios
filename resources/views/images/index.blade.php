@extends('layouts.master')

@section('titulo')
Hashtag AI
@endsection

@section('contenido')
<div class="album py-5 bg-light">
	<div class="container">
		<div class="row">
			@foreach( $imagenes as $imagen )
			<div class="col-md-4">
				<a href="{{ route('images.show' , $imagen) }}">
					<div class="card mb-4 box-shadow">
						<img class="card-img-top" src="{{ asset("assets/imagenes/") }}/{{ $imagen->imagen }}">
						<div class="card-body">
							<h4 class="card-text text-uppercase">{{ $imagen->titulo }}</h4>
							<div class="d-flex justify-content-between align-items-center">
								<div class="btn-group">
									<form action="{{ route('images.show' , $imagen) }}">
										<button type="button" class="btn btn-sm btn-outline-secondary">Ver resultados</button>
									</form>
								</div>
								<small class="text-muted">{{ $imagen->created_at }}</small>
							</div>
						</div>
					</div>
				</a>
			</div>
			@endforeach
		</div>
	</div>
</div>
@endsection


