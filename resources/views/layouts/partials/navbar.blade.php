<nav class="navbar navbar-expand-lg navbar-dark fixed-top bg-dark">
  <div class="container-fluid">
    <a class="navbar-brand" href="{{url('/')}}">Hashtag AI</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav me-auto mb-2 mb-lg-0">
        {{--@if(Auth::check() )--}}
        
        <li class="nav-item">
          <a href="{{route('images.index')}}" class="nav-link {{ request()->routeIs('images.*') && !request()->routeIs('images.create')? ' active' : ''}}">Imágenes</a>
        </li>
        <li class="nav-item">
          <a href="{{route('images.create')}}" class="nav-link {{ request()->routeIs('images.create')? ' active' : ''}}">Nuevo consulta</a>
        </li>
      </ul>
      {{--@endif --}}
  </div>
</div>
</nav>
