<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ImagesController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', [ImagesController::class,'index'])->name("home");

Route::get('images', [ImagesController::class,'index'])->name("images.index");

Route::get('images/crear', [ImagesController::class, 'create'])->name("images.create");

Route::get('images/{imagen}', [ImagesController::class, 'show'])->name("images.show");

Route::post('images', [ImagesController::class, 'store'])->name("images.store");