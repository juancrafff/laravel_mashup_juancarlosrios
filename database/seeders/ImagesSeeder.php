<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Imagen;
use Illuminate\Support\Str;

class ImagesSeeder extends Seeder
{
	private $rutas = array(
        array("titulo"=> "Cuidad random", "imagen" =>"ciudad.jpg"),
        array("titulo"=> "Cítricos", "imagen" =>"citricos.jfif"), 
        array("titulo"=> "Aprobando DWES con el Mashup", "imagen" =>"aprobado.jpg"),
        array("titulo"=> "Aventurero", "imagen" =>"explorador.jfif"),
        array("titulo"=> "Loro", "imagen" => "pajaro01.jpg"), 
        array("titulo"=> "Pajarraco", "imagen" =>"pajaro02.jpg"), 
        array("titulo"=> "Modelo", "imagen" =>"modelo.jpg"), 
        array("titulo"=> "Taxi", "imagen" =>"taxi.jpg"), 
        array("titulo"=> "Bebida", "imagen" =>"bebida.jpg"), 
        array("titulo"=> "Leopardo", "imagen" =>"leo.jfif")
    );
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	foreach ($this->rutas as $recursoNuevo) {
    		$img = new Imagen();
    		$img->titulo = $recursoNuevo["titulo"];
            $img->slug = Str::slug($img->titulo);
            $img->imagen = $recursoNuevo["imagen"];
            $img->save();
        }

        $this->command->info('Tabla de imágenes inicializada con datos');
    }
}

